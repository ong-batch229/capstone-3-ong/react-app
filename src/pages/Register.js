import { Form, Button } from "react-bootstrap"
import { useState, useEffect} from "react"
import { useNavigate } from 'react-router-dom'
import Swal from "sweetalert2"

export default function Register(){

	const navigate = useNavigate()

	// State Hooks -> store values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNum, setMobileNum] = useState("");
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	const register = () => {
		fetch("http://localhost:4000/users/register", {
			method: 'POST',
			body: JSON.stringify({
				email: email,
				password: password1,
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNum
			}),
			headers: {"Content-Type": "application/json"}
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){
				Swal.fire({
		            title: "Successfully Registered",
		            icon: "success",
		            text: "Please login to continue!"              
          	})
	            setEmail("")
	            setPassword1("")
	            setPassword2("")
	            navigate("/login")
        	} else {
         		Swal.fire({
		            title: "Something went wrong",
		            icon: "error",
		            text: "Please try again"
				})
			}
		})
	}
	const checkEmail = (e) => {
	e.preventDefault()

	fetch("http://localhost:4000/users/checkEmail", {
		method: "POST",
        body: JSON.stringify({ email }),
        headers: { "Content-Type": "application/json"}
	})
	.then(res => res.json())
	.then(data => {
		if(data === true){
			Swal.fire({
				title: "Email already exists",
				icon: "error",
				text: "Please input another email!"
			})
		}else{
			register()
		}
	})
	}

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		const all = email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNum !== '';
		const equalPassword = password1 === password2;
		const validNum = mobileNum.length > 10 && mobileNum.length < 12;

		if(all && equalPassword && validNum){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, mobileNum]);

	return(

/*		(user.id !== null) ?
		<Navigate to="/courses"/>
		:*/
		<Form onSubmit={checkEmail}>
		<h1>REGISTER</h1>
			<Form.Group className="mb-3" controlId="userFirstName">
        		<Form.Label>First Name</Form.Label>
        		<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
      		</Form.Group>
			<Form.Group className="mb-3" controlId="userMobileNum">
        		<Form.Label>Last Name</Form.Label>
        		<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
      		</Form.Group>
			<Form.Group className="mb-3" controlId="userMobileNum">
        		<Form.Label>Mobile Number</Form.Label>
        		<Form.Control type="number" placeholder="Enter Mobile Number" value={mobileNum} onChange={e => setMobileNum(e.target.value)} required/>
      		</Form.Group>
      		<Form.Group className="mb-3" controlId="userEmail">
        		<Form.Label>Email address</Form.Label>
        		<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
        		<Form.Text className="text-muted">We'll never share your email with anyone else.
        		</Form.Text>
      		</Form.Group>

     		<Form.Group className="mb-3" controlId="password1">
       			<Form.Label>Password</Form.Label>
        		<Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
      		</Form.Group>

      		<Form.Group className="mb-3" controlId="password2">
       			<Form.Label>Verify Password</Form.Label>
        		<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
      		</Form.Group>
			
			{/*CONDITIONAL RENDERING -> if ACTIVE BUTTON IS CLICKABLE -> if INACTIVE BUTTON IS NOT CLICKABLE*/}
      		{
      			(isActive) ?
      			<Button variant="primary" type="submit" controlId="submitBtn">Register
      			</Button>
      			:
      			<Button variant="primary" type="submit" controlId="submitBtn" disabled>Register
      			</Button>
      		}
   		</Form>
		)
}
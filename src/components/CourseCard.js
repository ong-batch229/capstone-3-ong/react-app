import {Card} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProps}){
	// Checks to see if the data was successfully passed
	console.log(courseProps);
	console.log(typeof courseProps);

	// Destructuring the data to avoid dot notation
	const {name, description, price, _id} = courseProps;

	// 3 Hooks in React
	// 1. useState
	// 2. useEffect
	// 3. useContext

	// Use the useState hook for the component to be able to store state
	// States are used to keep track of information related to individual components
	// Syntax -> const [getter, setter] = useState(initialGetterValue);

	const [count] = useState(0);
	const [seats] = useState(30);


	/*function enroll(){
		if(seats > 0) {
			setSeats(seats - 1);
			console.log("Enrollees "+ count);
			setCount(count + 1);
			console.log("Seats "+ seats);
		}else{
			alert("No more seats!")
		}
	}*/

	return(
		<>
			<Card className="p-0 m-4">
				<Card.Body>
						<h4 className="text-bold mb-0">{name}</h4>
						<p className="m-0">Description:</p>
						<p className="mb-2">{description}</p>
						<p className="m-0">Price:</p>
						<p className="m-0">{price}</p>
						<p className="mt-3">Enrollees: {count}</p>
						<p className="mt-3">Slots Available: {seats}</p>
						<Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
				</Card.Body>
			</Card>
		</>
	)
}